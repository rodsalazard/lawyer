<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesAudienceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_audience', function (Blueprint $table) {
            $table->increments('id');
            $table->string('applicant');
            $table->string('defendant');
            $table->string('type');
            $table->string('court');
            $table->string('num_expedient');
            $table->string('property_type');
            $table->mediumText('address');
            $table->decimal('appraisal',10,2);
            $table->dateTime('audience_date');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_audience');
    }
}
