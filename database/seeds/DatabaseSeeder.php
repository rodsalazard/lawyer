<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
        'name' => 'Administrador',
        ]);
        
        DB::table('users')->insert([
        'name' => 'John Doe',
        'email' => 'admin@admin.com',
        'password' => '$2y$10$5qXnkY5GgUjwfpGei0fqM..aFv/MWJe4YNpyznwiPNlpArVJXw05a',
        'role_id' => 1,
        ]);



        // DB::table('judgments')->insert([
        // 'user_id' => 1,
        // 'num_expedient' => '233232/28',
        // 'applicant' => 'Luis Hernandez',
        // 'defendant' => 'Lh',
        // 'type' => 'dfds',
        // 'court' => 'Juez primero',
        // ]);
        //
        //
        // DB::table('agreements')->insert([
        // 'user_id' => 1,
        // 'judgment_id' => 1,
        // 'dictate_date' => '2018-09-30',
        // 'publish_date' => '2018-09-30',
        // 'description' => 'sdasdasdasdasd',
        // 'type' => 'asdad',
        // ]);



    }
}
