<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');

Route::group([
	'middleware' => 'auth',
], function() {

	Route::get('/', function () {
        return view('dashboard');
    });

	Route::group([
        'prefix' => 'users'
    ], function () {
		Route::get('/', 'UsersController@index');
        Route::get('/create', 'UsersController@create');
        Route::get('/edit/{user}', 'UsersController@create');
		Route::post('/update/{user}', 'UsersController@update');
		Route::post('/store', 'UsersController@store');
    });

	Route::group([
        'prefix' => 'clients'
    ], function () {
		Route::get('/', 'ClientsController@index');
        Route::get('/create', 'ClientsController@create');
        Route::get('/edit/{user}', 'ClientsController@create');
		Route::post('/update/{user}', 'ClientsController@update');
		Route::post('/store', 'ClientsController@store');
    });

});
