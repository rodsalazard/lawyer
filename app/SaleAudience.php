<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleAudience extends Model{

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'applicant','defendant','type','court','num_expedient','property_type','address','appraisal','audience_date'
    ];

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * Relationship roles
     *
     **/
    public function agreements(){
        return $this->hasMany('App\Agreement');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    /**
     * Get all of the post's comments.
     */
    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    /**
     * Get all of the post's comments.
     */
    public function files()
    {
        return $this->morphMany('App\File', 'owner');
    }

}
