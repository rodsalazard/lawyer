<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','image','password','phone','address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'role','judgments'
    ];

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * Relationship roles
     *
     **/
    public function role(){
        return $this->belongsTo('App\Role');
    }

    /**
     * Relationship roles
     *
     **/
    public function judgments(){
        return $this->hasMany('App\Judgment');
    }

    public function getRoleAttribute()
    {
        return $this->role()->first();
    }

    public function getJudgmentsAttribute()
    {
        return $this->judgments()->get();
    }


}
