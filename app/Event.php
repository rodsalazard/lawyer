<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model{

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'agreement_id','start','end','description','notify','location'
    ];

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * Relationship roles
     *
     **/
    public function agreement(){
        return $this->belongsTo('App\Agreement');
    }

    /**
     * Get all of the post's comments.
     */
    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

}
