<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agreement extends Model{

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'judgment_id','dictate_date','publish_date','description','type'
    ];

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * Relationship roles
     *
     **/
    public function judgment(){
        return $this->belongsTo('App\Judgment');
    }

    public function events(){
        return $this->hasMany('App\Agreement');
    }

    /**
     * Get all of the post's comments.
     */
    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    /**
     * Get all of the post's comments.
     */
    public function files()
    {
        return $this->morphMany('App\File', 'owner');
    }

}
