<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;

class UsersController extends Controller
{
    public function __construct()
    {
        // ini_set('upload_max_filesize', '3M');
        // ini_set('post_max_size', '8M');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index', ['users' => User::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user = null)
    {
        $data = $user ? ['user' => $user]:[];
        $data['roles'] = Role::all();
        return view('users.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $photo = $request->file('photo');

        $save_user = [
            'name' => $request->input('name') ? $request->input('name'):'',
            'email' => $request->input('email') ? $request->input('email'):'',
            'phone' => $request->input('phone') ? $request->input('phone'):'',
            'rol_id' => $request->input('rol_id') ? $request->input('rol_id'):'',
        ];

        if($request->input('password')){
            $save_user['password'] = bcrypt($request->input('password'));
        }

        if($photo){
            $image_name = sha1($photo->getClientOriginalName().time()).'.'.$photo->getClientOriginalExtension();
            $photo->move(public_path('/images/users'), $image_name);
            $save_user['image'] = $image_name;
        }

        User::firstOrNew($save_user)->save();

        return redirect('/users')->with('status', 'Perfil actualizado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $photo = $request->file('photo');

        $save_user = [
            'name' => $request->input('name') ? $request->input('name'):'',
            'email' => $request->input('email') ? $request->input('email'):'',
            'phone' => $request->input('phone') ? $request->input('phone'):'',
        ];

        if($request->input('password')){
            $save_user['password'] = bcrypt($request->input('password'));
        }

        if($photo){
            $image_name = sha1($photo->getClientOriginalName().time()).'.'.$photo->getClientOriginalExtension();
            $photo->move(public_path('/images/users'), $image_name);
            $save_user['image'] = $image_name;
        }

        $user->fill($save_user)->save();

        return redirect('/users')->with('status', 'Perfil actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
