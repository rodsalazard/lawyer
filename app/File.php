<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model{

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'owner_id','file_name','file_size','file_path','file_extension','owner_type'
    ];

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * Get all of the owning commentable models.
     */
    public function owner()
    {
        return $this->morphTo();
    }

}
