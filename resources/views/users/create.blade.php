@extends('app')

@section('content')
<div class="page-title">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel" style="height:600px;">
                <div class="x_title">
                    @if(isset($user))
                        <h2>Editar información de <i>"{{ $user->name }}"</i></h2>
                    @else
                        <h2>Crear nuevo usuario</h2>
                    @endif
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(isset($user))
                        <form-user token="{{csrf_token()}}" :user='{!! json_encode($user) !!}' :roles='{!! json_encode($roles) !!}'></form-user>
                    @else
                        <form-user token="{{csrf_token()}}" :roles='{!! json_encode($roles) !!}'></form-user>
                    @endif
                </div>
            </div>
        </div>
    </div>



</div>
@endsection
