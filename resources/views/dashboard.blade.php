@extends('app')

@section('content')
<div class="page-title">
    <div class="title_left">
        <h3>Dashboard</h3>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel" style="height:600px;">
                <div class="x_title">
                    <h2>Titulo de panel {{ Config::get('constants.JUDGMENTS_COMMENTS') }}</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                    </div>
                </div>
            </div>
        </div>
    </div>



</div>
@endsection
