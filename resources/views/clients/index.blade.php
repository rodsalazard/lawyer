@extends('app')

@section('content')
<div class="page-title">
    {{-- <div class="title_left">
        <h3>Usuarios</h3>
    </div>
    <div class="clearfix"></div> --}}
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel" style="height:600px;">
                <div class="x_title">
                    <h2>Cartera de clientes</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <clients :data='{!! @json_encode($clients) !!}'></clients>
                </div>
            </div>
        </div>
    </div>



</div>
@endsection

{{-- @push('styles')
    {!! Html::style('vendors/iCheck/skins/flat/green.css') !!}
@endpush

@push('scripts')
    {!! Html::script('vendors/iCheck/icheck.min.js') !!}
@endpush --}}
