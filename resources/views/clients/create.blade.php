@extends('app')

@section('content')
    <div class="page-title">

        @if(isset($client))
            <form-client token="{{csrf_token()}}" :client='{!! json_encode($client) !!}'></form-client>
        @else
            <form-client token="{{csrf_token()}}"></form-client>
        @endif
        
    </div>
@endsection
