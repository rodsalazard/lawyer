
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/**
 * Components for users
 */
Vue.component('users', require('./components/users/Users.vue'));
Vue.component('form-user', require('./components/users/FormUser.vue'));

/**
 * Components for clients
 */
Vue.component('clients', require('./components/clients/Clients.vue'));
Vue.component('form-client', require('./components/clients/FormClient.vue'));
Vue.component('list-judgments', require('./components/clients/ListJudgments.vue'));




const app = new Vue({
    el: '#app'
});
